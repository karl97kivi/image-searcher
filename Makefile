CC=gcc
CFLAGS=-Wall -g 

all:imgsearcher

imgsearcher:image_searcher.o
	$(CC) $(CFLAGS) image_searcher.o -lexif -o image_searcher

%.o: %.c 
	$(CC) $(CFLAGS) -c $^ 

test:
	$(CC) $(CFLAGS) -c test.c
	$(CC) $(CFLAGS) test.o -o test

###########################
# 	INPUT filter testing  
###########################
	# TEST 1	(exact file match)
	./image_searcher -f ABTEST0012.jpg > Test/test_1.txt
	# TEST 1a	(exact capture date)
	./image_searcher -f ABTEST0012.jpg -t "2008:11:01 21:15:07" > Test/test_1a.txt
	# TEST 1b	(exact camera model)
	./image_searcher -f ABTEST0012.jpg -m "COOLPIX P6000" > Test/test_1b.txt
	# TEST 1c	(partial camera model)
	./image_searcher -f ABTEST0012.jpg -m "COOLPIX*" > Test/test_1c.txt
	######
	# TEST 2	(case insensitive, exact match)
	./image_searcher -f ABtest0012.jpg > Test/test_2.txt
	# TEST 2a 	(exact capture date)
	./image_searcher -f ABtest0012.jpg -t "2008:11:01 21:15:07" > Test/test_2a.txt
	# TEST 2b	(exact camera model)
	./image_searcher -f ABtest0012.jpg -m "COOLPIX P6000" > Test/test_2b.txt
	# TEST 2c	(partial camera model)
	./image_searcher -f ABtest0012.jpg -m "COOLPIX*" > Test/test_2c.txt
	######
	# TEST 3	(partial match)
	./image_searcher -f ABTEST\* > Test/test_3.txt
	# TEST 3a 	(exact capture date)
	./image_searcher -f ABTEST\* -t "2008:11:01 21:15:07" > Test/test_3a.txt
	# TEST 3b	(exact camera model)
	./image_searcher -f ABTEST\* -m "COOLPIX P6000" > Test/test_3b.txt
	# TEST 3c	(partial camera model)
	./image_searcher -f ABTEST\* -m "COOLPIX*" > Test/test_3c.txt

	# TEST 4	(case insensitive, partial match)
	./image_searcher -f ABtest\* > Test/test_4.txt
	# TEST 4a 	(exact capture date)
	./image_searcher -f ABtest\* -t "2008:11:01 21:15:07" > Test/test_4a.txt
	# TEST 4b	(exact camera model)
	./image_searcher -f ABtest\* -m "COOLPIX P6000" > Test/test_4b.txt
	# TEST 4c	(partial camera model)
	./image_searcher -f ABtest\* -m "COOLPIX*" > Test/test_4c.txt

	# TEST 5	(exact match)
	./image_searcher -f ./ABTEST0012.jpg > Test/test_5.txt
	# TEST 5a 	(exact capture date)
	./image_searcher -f ./ABTEST0012.jpg -t "2008:11:01 21:15:07" > Test/test_5a.txt
	# TEST 5b	(exact camera model)
	./image_searcher -f ./ABTEST0012.jpg -m "COOLPIX P6000" > Test/test_5b.txt
	# TEST 5c	(partial camera model)
	./image_searcher -f ./ABTEST0012.jpg -m "COOLPIX*" > Test/test_5c.txt

	# TEST 6	(case insensitive, exact match)
	./image_searcher -f ./ABtest0012.jpg > Test/test_6.txt
	# TEST 6a 	(exact capture date)
	./image_searcher -f ./ABtest0012.jpg -t "2008:11:01 21:15:07" > Test/test_6a.txt
	# TEST 6b	(exact camera model)
	./image_searcher -f ./ABtest0012.jpg -m "COOLPIX P6000" > Test/test_6b.txt
	# TEST 6c	(partial camera model)
	./image_searcher -f ./ABtest0012.jpg -m "COOLPIX*" > Test/test_6c.txt

	# TEST 7	(partial match)
	./image_searcher -f ./ABTEST\* > Test/test_7.txt
	# TEST 7a 	(exact capture date)
	./image_searcher -f ./ABTEST\* -t "2008:11:01 21:15:07" > Test/test_7a.txt
	# TEST 7b	(exact camera model)
	./image_searcher -f ./ABTEST\* -m "COOLPIX P6000" > Test/test_7b.txt
	# TEST 7c	(partial camera model)
	./image_searcher -f ./ABTEST\* -m "COOLPIX*" > Test/test_7c.txt

	# TEST 8	(case insensitive, partial match)
	./image_searcher -f ./ABtest\* > Test/test_8.txt
	# TEST 8a 	(exact capture date)
	./image_searcher -f ./ABtest\* -t "2008:11:01 21:15:07" > Test/test_8a.txt
	# TEST 8b	(exact camera model)
	./image_searcher -f ./ABtest\* -m "COOLPIX P6000" > Test/test_8b.txt
	# TEST 8c	(partial camera model)
	./image_searcher -f ./ABtest\* -m "COOLPIX*" > Test/test_8c.txt

	# TEST 9	(directory - it has '/' so it behaves as any other directory)
	./image_searcher -f ./TestPictures > Test/test_9.txt
	# TEST 9 a	(exact capture date)
	./image_searcher -f ./TestPictures -t "2008:11:01 21:15:07" > Test/test_9a.txt
	# TEST 9b	(exact camera model)
	./image_searcher -f ./TestPictures -m "COOLPIX P6000" > Test/test_9b.txt
	# TEST 9c	(partial camera model)
	./image_searcher -f ./TestPictures -m "C*" > Test/test_9c.txt

	# TEST 10	(current directory)
	./image_searcher -f TestPictures > Test/test_10.txt	
	# TEST 10 a	(exact capture date)
	./image_searcher -f TestPictures -t "2008:11:01 21:15:07" > Test/test_10a.txt
	# TEST 10b	(exact camera model)
	./image_searcher -f TestPictures -m "COOLPIX P6000" > Test/test_10b.txt
	# TEST 10c	(partial camera model)
	./image_searcher -f TestPictures -m "C*" > Test/test_10c.txt
	#TEST 11	(check string null termination)
	./image_searcher -f ./TestPictures/Fujifilm_finepix\* > Test/test_11.txt


	./test


clean:
	rm *.o image_searcher test Test/test*