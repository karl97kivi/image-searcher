#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int compareFiles(char * file1, char * file2);

int main(int argc, char **argv) {
    /* validated tests */
    char valid_test[100][256];
    strcpy(valid_test[0],"Test/valid_test1.txt");
    strcpy(valid_test[1],"Test/valid_test1a.txt");
    strcpy(valid_test[2],"Test/valid_test1b.txt");
    strcpy(valid_test[3],"Test/valid_test1c.txt");

    strcpy(valid_test[4],"Test/valid_test2.txt");
    strcpy(valid_test[5],"Test/valid_test2a.txt");
    strcpy(valid_test[6],"Test/valid_test2b.txt");
    strcpy(valid_test[7],"Test/valid_test2c.txt");

    strcpy(valid_test[8],"Test/valid_test3.txt");
    strcpy(valid_test[9],"Test/valid_test3a.txt");
    strcpy(valid_test[10],"Test/valid_test3b.txt");
    strcpy(valid_test[11],"Test/valid_test3c.txt");


    strcpy(valid_test[12],"Test/valid_test4.txt");
    strcpy(valid_test[13],"Test/valid_test4a.txt");
    strcpy(valid_test[14],"Test/valid_test4b.txt");
    strcpy(valid_test[15],"Test/valid_test4c.txt");


    strcpy(valid_test[16],"Test/valid_test5.txt");
    strcpy(valid_test[17],"Test/valid_test5a.txt");
    strcpy(valid_test[18],"Test/valid_test5b.txt");
    strcpy(valid_test[19],"Test/valid_test5c.txt");


    strcpy(valid_test[20],"Test/valid_test6.txt");
    strcpy(valid_test[21],"Test/valid_test6a.txt");
    strcpy(valid_test[22],"Test/valid_test6b.txt");
    strcpy(valid_test[23],"Test/valid_test6c.txt");


    strcpy(valid_test[24],"Test/valid_test7.txt");
    strcpy(valid_test[25],"Test/valid_test7a.txt");
    strcpy(valid_test[26],"Test/valid_test7b.txt");
    strcpy(valid_test[27],"Test/valid_test7c.txt");


    strcpy(valid_test[28],"Test/valid_test8.txt");
    strcpy(valid_test[29],"Test/valid_test8a.txt");
    strcpy(valid_test[30],"Test/valid_test8b.txt");
    strcpy(valid_test[31],"Test/valid_test8c.txt");


    strcpy(valid_test[32],"Test/valid_test9.txt");
    strcpy(valid_test[33],"Test/valid_test9a.txt");
    strcpy(valid_test[34],"Test/valid_test9b.txt");
    strcpy(valid_test[35],"Test/valid_test9c.txt");


    strcpy(valid_test[36],"Test/valid_test10.txt");
    strcpy(valid_test[37],"Test/valid_test10a.txt");
    strcpy(valid_test[38],"Test/valid_test10b.txt");
    strcpy(valid_test[39],"Test/valid_test10c.txt");

    strcpy(valid_test[40],"Test/valid_test11.txt");


    /* program output */
    char test[100][256];
    strcpy(test[0],"Test/test_1.txt");
    strcpy(test[1],"Test/test_1a.txt");
    strcpy(test[2],"Test/test_1b.txt");
    strcpy(test[3],"Test/test_1c.txt");

    strcpy(test[4],"Test/test_2.txt");
    strcpy(test[5],"Test/test_2a.txt");
    strcpy(test[6],"Test/test_2b.txt");
    strcpy(test[7],"Test/test_2c.txt");

    strcpy(test[8],"Test/test_3.txt");
    strcpy(test[9],"Test/test_3a.txt");
    strcpy(test[10],"Test/test_3b.txt");
    strcpy(test[11],"Test/test_3c.txt");

    strcpy(test[12],"Test/test_4.txt");
    strcpy(test[13],"Test/test_4a.txt");
    strcpy(test[14],"Test/test_4b.txt");
    strcpy(test[15],"Test/test_4c.txt");


    strcpy(test[16],"Test/test_5.txt");
    strcpy(test[17],"Test/test_5a.txt");
    strcpy(test[18],"Test/test_5b.txt");
    strcpy(test[19],"Test/test_5c.txt");


    strcpy(test[20],"Test/test_6.txt");
    strcpy(test[21],"Test/test_6a.txt");
    strcpy(test[22],"Test/test_6b.txt");
    strcpy(test[23],"Test/test_6c.txt");


    strcpy(test[24],"Test/test_7.txt");
    strcpy(test[25],"Test/test_7a.txt");
    strcpy(test[26],"Test/test_7b.txt");
    strcpy(test[27],"Test/test_7c.txt");


    strcpy(test[28],"Test/test_8.txt");
    strcpy(test[29],"Test/test_8a.txt");
    strcpy(test[30],"Test/test_8b.txt");
    strcpy(test[31],"Test/test_8c.txt");


    strcpy(test[32],"Test/test_9.txt");
    strcpy(test[33],"Test/test_9a.txt");
    strcpy(test[34],"Test/test_9b.txt");
    strcpy(test[35],"Test/test_9c.txt");


    strcpy(test[36],"Test/test_10.txt");
    strcpy(test[37],"Test/test_10a.txt");
    strcpy(test[38],"Test/test_10b.txt");
    strcpy(test[39],"Test/test_10c.txt");

    strcpy(test[40],"Test/test_11.txt");

    
    int fail = 0;
    int success = 0;
    int total = 41;

    for (int i=0; i<total;i++) {
        int match = compareFiles(valid_test[i], test[i]);
        if (match == 0) {
            success++;
            printf("test nr:%d, %s successful\n",i+1,test[i]);
        }
        else {
            fail++;
            printf("test nr:%d, %s failed\n",i+1,test[i]);
        }
        
    }
    printf("Total %d tests: %d successful, %d failed\n", total, success, fail);
}

int compareFiles(char * file1, char * file2) {

    FILE *f1 = fopen(file1, "r");
    FILE *f2 = fopen(file2, "r");

    if (f1 == NULL || f2 == NULL) {
        printf("No such files\n");
        return -1;
        //exit(EXIT_FAILURE);
    }

    char ch1, ch2;

    do {
        ch1 = fgetc(f1);
        ch2 = fgetc(f2);

        if (ch1 != ch2) return -1;
    } while (ch1 != EOF && ch2 != EOF);
    /*if match then 0*/
    if (ch1 == EOF && ch2 == EOF) return 0;
    else return -1;
    fclose(f1);
    fclose(f2);
}