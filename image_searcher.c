/*
*   Author: Karl Kivi
*   Date: 27.04.2020
*/
#include "image_searcher.h"

int main(int argc, char**argv) {

    struct stat sb_path;
    /* determine which arguments user defined */
    check_arguments(argc, argv);
    if (strcmp(filepath, "/") == 0 || strcmp(filepath, "/.") == 0) {
        exit(-1);
    }
    
    /* find all jpg/jpeg files with defined camera model and capture date */
    if (file_flag == 1) {
        if (lstat(filepath, &sb_path) != -1)
            /* case-sensitive search for a file */
            valid_filepath(sb_path, filepath);
        else
            /* case-insensitive search for a file */
            invalid_filepath(filepath);

        /* Filter by model */
        if (model_flag == 1) filter_model(model);
        /* Filter by capture date */
        if (time_flag == 1) filter_capture_time(date);
        /* display result */
        print_images();
    }
    return 0;
}

void valid_filepath(struct stat sb_path, char*filepath) {
    /* file path is directory */
    if (S_ISDIR(sb_path.st_mode)){
        find_all_files(filepath);
        /* lowercase found file names */
        lowercase_image_struct(1);
    }
    /* file path is regular file */
    if (S_ISREG(sb_path.st_mode)){
        /* get file name from filepath */
        char * filename = strrchr(filepath, (char) '/');
        if (filename) {
            /* file not in current directory */
            find_jpeg_jpg(filename+1, filepath,1);
        } else {
            /* file in current directory */
            find_jpeg_jpg(filepath, filepath,1);
        }
        /* lowercase found file names */
        lowercase_image_struct(1);
    }
}

void invalid_filepath(char *filepath) {
    /* making search case-insensitive */
    /* find all files in invalid filename folder */
    char *isdirectory = strrchr(filepath, '/');
    if (isdirectory) {
        /* get filename folder path */
        char path[256];
        strcpy(path, filepath);
        for (int i=0; i<strlen(isdirectory+1);i++) {
            path[strlen(path)-1] = '\0';
        }
        /*check 'path' is directory */
        DIR *dir = opendir(path);
        if (dir) find_all_files(path);  
    } else {
        /* find all files in current directory */
        find_all_files("./");
    }
    /* check file or folder even exists */
    int exists = check_file_exists(filepath);
    if (exists == -1) {
        fprintf(stderr, "'%s': %s\n",filepath, strerror(errno));
        exit(-1);
    }
}

void print_images() {
    /* print header */
    print_header();
    /* print filtered data */
    int i=0;
    while (i < imageInfo.size) {
        if (imageInfo.data[i].printFlag){
            printf("%-40s", imageInfo.data[i].file);
            printf("%-30s", imageInfo.data[i].time);
            printf("%s\n", imageInfo.data[i].model);
        }    
        i++;
    }
}


void check_arguments(int argc, char **argv) {
    if (argc < 2) {
        printf("Try './image_searcher --help' for more information.\n");
    }
    while (--argc) {
        if (strcmp(argv[argc], "-f")==0) {
            if (argv[argc+1] != NULL) {
                filepath = argv[argc+1];
                file_flag = 1;
            } else {
                printf("Try './image_searcher --help' for more information.\n");
            }
        }
        else if (strcmp(argv[argc], "-t")==0) {
            time_flag = 1;
            if (argv[argc+1] != NULL) {
                date = argv[argc+1];
                time_flag = 1;
            } else {
                printf("Try './image_searcher --help' for more information how to filter by capture date.\n");
            }
        }
        else if (strcmp(argv[argc], "-m")==0) {
            if (argv[argc+1] != NULL) {
                model = argv[argc+1];
                model_flag = 1;
            } else {
                printf("Try './image_searcher --help' for more information how to filter by camera model.\n");
            }
        }
        else if (strcmp(argv[argc], "-h")==0 || strcmp(argv[argc], "--help")==0) {
            help_page();
            exit(-1);
        } 
    }
}

void help_page() {
    printf("Usage: ./image_searcher -f path/to/file [-m \"camera model\"] [-t \"capture time\"]\n");
    printf("\nMandatory: \n");
    printf("\t-f\t\tuse to define path to file or folder\n");
    printf("\nOptional\n");
    printf("\t-m\t\tuse to filter exact or partial camera model\n");
    printf("\t-t\t\tuse to filter exact capture date.\n\n");
    printf("Notes:\n");
    printf("\tPartial camera model or filename search must be between double quotes. Example 2 and 4.\n");
    printf("\tCapture time format: \"YYYY:MM:DD hh:mm:ss\"\n");
    printf("\tImage file searching is case-insensitive.\n");
    
    printf("\nExamples:\n");
    printf("\tExample 1: ./image_searcher -f Pictures/things\n");
    printf("\tExample 2: ./image_searcher -f \"../partial_filename*\" -m \"NIKON D90\"\n");
    printf("\tExample 3. ./image_searcher -f ./ -m \"NIKON D90\" -t \"2008:11:01 21:15:09\"\n");
    printf("\tExample 4: ./image_searcher -f ./ -m \"NIKO*\"\n");
    


}

void print_header() {
    /* find atleast one item that has print flag 1 otherwise do not print header */
    int print_flag = 0;
    for (int i=0;i<imageInfo.size;i++) {
        if (imageInfo.data[i].printFlag == 1) print_flag = 1;
    }

    if (print_flag == 1) {
        /* define column names */
        char file_string[256] = "File";
        char time_string[256] = "Capture time";
        char model_string[256] = "Camera model";
        /* print names with formatting */
        for (int i=0;i<20;i++) printf("-----");
        printf("\n");
        printf("%-40s%-30s%s\n",file_string, time_string, model_string);
        for (int i=0;i<20;i++) printf("-----");
        printf("\n");
    }
    
}

void find_all_files(const char *filepath) {
    /* pointer for directory entry */
    struct dirent *de;
    struct stat sb;
    /* check directory */
    DIR *dr = opendir(filepath);
    while ((de = readdir(dr)) != NULL) {
        /* concatenate two strings: filepath + new file/dir name */
        char new_filepath[257];
        snprintf(new_filepath, sizeof new_filepath, "%s/%s", filepath, de->d_name);
        /* get file or dir stats */
        if (lstat(new_filepath, &sb) != -1) {
            if (S_ISDIR(sb.st_mode)){
                /* sort out folders . and .. */
                if (!(strcmp(de->d_name, "..") == 0 || strcmp(de->d_name, ".") == 0)) {
                    find_all_files(new_filepath);
                } else {
                    //printf("found . :%s\n", de->d_name);
                }    
            }
            else if (S_ISREG(sb.st_mode)){
                find_jpeg_jpg(de->d_name,new_filepath,1);
            }       
        } else {
            /* if lstat function fails */
            fprintf(stderr, "'%s': %s\n", de->d_name, strerror(errno));
        }
    }
    closedir(dr);
}

void find_jpeg_jpg(char *filename, char *filepath, int print_flag) {
    static int count = 0;
    /* returns a pointer to the last occurrence of the character '.' in the filename */
    char * ext = strrchr(filename, (char) '.');
    if (ext) {
        if (strcmp(ext, ".jpeg") == 0 || strcmp(ext, ".jpg") == 0) {
            /* save filename into struct */
            strcpy(imageInfo.data[count].file, filename);
            /* decide if data is displayable by writing 0 or 1 */
            imageInfo.data[count].printFlag = print_flag;
            /* increment struct size */
            imageInfo.size = ++count;
            /* find exif data and store into struct */
            find_exif(filepath);
            
        }
            
    }
}

void find_exif(const char * filename) {
    /* get jpg/jpeg image metadata */
    ExifData * jpeg_exif = exif_data_new_from_file(filename);
    if (jpeg_exif != NULL) {
        ExifEntry * entryDate = exif_data_get_entry(jpeg_exif, EXIF_TAG_DATE_TIME);
        ExifEntry * entryModel = exif_data_get_entry(jpeg_exif, EXIF_TAG_MODEL);
        if (entryDate != NULL) {
            /* store capture date into struct */
            strcpy(imageInfo.data[imageInfo.size-1].time, (const char *) entryDate->data);
        } else {
            /* write empty string if capture date is absent in jpg/jpeg image */
            strcpy(imageInfo.data[imageInfo.size-1].model, (const char *) " ");
        }
        if (entryModel != NULL) {
            /* store camera model into struct */
            strcpy(imageInfo.data[imageInfo.size-1].model, (const char *) entryModel->data);
        } else {
            /* write empty string if camera model is absent in jpg/jpeg image */
            strcpy(imageInfo.data[imageInfo.size-1].model, (const char *) " ");
        }
    }
}



int check_file_exists(char * filepath) {
    int image_exists = 0;
    /* lowercase all struct characters */
    lowercase_image_struct(0);
    /* lowercase user input */
    char * filename = strrchr(filepath, (char) '/');
    char lowered_filename[256];      
    /* check file location: current folder/somewhere else */
    if (filename) {
        /* somewhere else */
        for (int i=0;i<=strlen(filename+1);i++) 
            lowered_filename[i] = tolower(filename[i+1]);    
    } else {
        /* current folder */
        for (int i=0;i<=strlen(filepath);i++) 
            lowered_filename[i] = tolower(filepath[i]);   
    }

    /* compare lowercased user input and folder content */
    for (int h=0;h<imageInfo.size;h++) {
        int matchfound = 1;
        for (int g=0;g<strlen(lowered_filename);g++) {
            if (lowered_filename[g] != '*') {
                if (lowered_filename[g] != lowercaseImg[h].file[g]) {
                    matchfound=0;
                }
            }
        }
        /* set matched data displayable in struct */
        if (matchfound == 1) {
            image_exists = 1;
            imageInfo.data[h].printFlag = 1;
        }
    }
    /* file check result */
    if (image_exists == 1) return 0;
    else return -1;
}

void filter_model(char *model) {
    /* lowercase user input from -m option */
    char lowered_model[256];
    for (int i=0;i<=strlen(model);i++) {
        lowered_model[i] = tolower(model[i]);
    }
    /* filter found jpg/jpeg files with case-insensitive input from user */
    filter_data(model, lowered_model, MODEL_FLAG);
}

void filter_capture_time(char *date) {
    /* filter capture date from -t option */
    filter_data(date, date, TIME_FLAG);
}

void filter_data(char *t_data, char*lowercase_data,  enum OPTION_FLAGS flag) {
    for (int h=0;h<imageInfo.size;h++) {
        /* filter only image struct data that is displayable */
        if (imageInfo.data[h].printFlag == 1) {
            /* compare two lowercased strings */
            int matchfound = 1;
            for (int g=0;g<strlen(t_data);g++) {
                /* compare until * is found */
                if (lowercase_data[g] != '*') {
                    if (flag == TIME_FLAG) {
                        if (lowercase_data[g] != lowercaseImg[h].time[g]) matchfound=0;
                    }
                    if (flag == MODEL_FLAG) {
                        if (lowercase_data[g] != lowercaseImg[h].model[g]) matchfound=0;
                    }
                }
            }
            /* not matched data is declared as not displayable */
            if (matchfound == 0) imageInfo.data[h].printFlag = 0;  
        }
    }
}

void lowercase_image_struct(int initial_flag) {
    /* making copy of original struct data */
    memcpy(lowercaseImg, imageInfo.data, sizeof(lowercaseImg));
    /* lowercase all data for case-insensitive filtering */
    for (int i=0;i<imageInfo.size;i++) {
        /* set all data not displayable */
        imageInfo.data[i].printFlag = initial_flag;
        /* lowercase all filenames and camera models */
        for (int j=0;j<=strlen(lowercaseImg[i].file);j++) {
            lowercaseImg[i].file[j] = tolower(imageInfo.data[i].file[j]);
        }
        for (int k=0;k<=strlen(lowercaseImg[i].model);k++) {
            lowercaseImg[i].model[k] = tolower(imageInfo.data[i].model[k]);
        }
    }
}

