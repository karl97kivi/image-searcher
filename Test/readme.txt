Testing with files (11 tests)

Run this test:
#make clean
#make
#make test

Examle file: Canon_40D.jpg

### In the same folder
Canon_40D.jpg			(exact match)
canon_40D.jpg			(case insensitive, exact match)
Can\*				(partial match)
can\*				(case insensitive, partial match)

### Not in the same folder
../../Canon_40D.jpg		(exact match)
../../canon_40D.jpg		(case insensitive, exact match)
../../Can\*			(partial match)
../../can\*			(case insensitive, partial match)

### Files in folders
./folder			(current directory)
folder				(current directory)

