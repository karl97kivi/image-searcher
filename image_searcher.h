/*
*   Author: Karl Kivi
*   Date: 27.04.2020
*/
#include <stdio.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>
#include <sys/mman.h>
#include <ctype.h>
#include <stdlib.h>
#include <libexif/exif-data.h>
#include <libexif/exif-loader.h>


enum OPTION_FLAGS {
    FILE_FLAG=0,
    TIME_FLAG=1,
    MODEL_FLAG=2
};

/* save found files data */
typedef struct {
    char file[256];
    char time[256];
    char model[256];
    int printFlag;
} Image;

typedef struct {
    Image data[256];
    int size;
} ImageInfo;

char *filepath;
char *model;
char *date;
int file_flag = 0;
int time_flag = 0;
int model_flag = 0;

/* defining image data struct */
ImageInfo imageInfo;
/* defining lowercased image data struct */
Image lowercaseImg[256];


void print_header();
void print_images();
void help_page();
void check_arguments(int argc, char **argv);

void valid_filepath(struct stat sb_path, char*filepath);
void invalid_filepath(char *filepath);

void find_all_files(const char *filepath);
void find_jpeg_jpg(char *filename, char *filepath, int print_flag);
void find_exif(const char * filename);

int check_file_exists(char * filepath);

void filter_capture_time(char *date);
void filter_model(char *model);
void filter_data(char *t_data, char*lowercase_data, enum OPTION_FLAGS flag);

void lowercase_image_struct(int initial_flag);

