# Implementing Image Searcher v1.0.0

### Overview
Simple console utility to perform search over a folder and its subfolders that contain JPEG image files. Using common POSIX concepts for user experience.

### Notes
* Capture date can also be inserted partially
* Partial file name must be inserted between double quotes. Look at example shown below.

### Installation of libexif
Guide is on this site:
http://www.linuxfromscratch.org/blfs/view/svn/general/libexif.html

### Compile
Simply run make. 
```sh
$ make
```
Cleaning binary and object files run make clean:
```sh
$ make clean
```
### Run Image Searcher
```sh
$ ./image_seacher -f path/to/file
```

### Examples
Display all jpeg/jpg files in current directory
```sh
$ ./image_searcher -f ./
```
Display all jpeg/jpg files with partial file name.
```sh
$ ./image_searcher -f "./partial_filename*"
```
Display all jpeg/jpg files with exact camera model
```sh
$ ./image_searcher -f ./picturefolder/filename -m "NIKON D300" 
```
Display all jpeg/jpg files with partial camera model
```sh
$ ./image_searcher -f ./picturefolder/filename -m "NIK" 
```
Display all jpeg/jpg files with exact capture date
```sh
$ ./image_searcher -f ./TestPictures -t "2008:07:31 10:03:44"
```
Display all jpeg/jpg files with partial capture date
```sh
$ ./image_searcher -f ./TestPictures -t "2008:07"
```
Define both capture date and camera model
```sh
$ ./image_searcher -f ./TestPictures -m "NIk" -t "2008:07:31"
```
### Tests
Testing different user input cases. Run tests with make:
```sh
$ make clean
$ make
$ make test
```

### Copyright
Unless stated otherwise, all images come from Wikipedia Commons - http://commons.wikimedia.org
For author and license information please refer to their respective description pages.